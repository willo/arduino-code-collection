#include <VirtualWire.h>
int RF_TX_PIN = 2;
int sensorPin = 0;
const int buttonPin = 7;
int buttonState = 0;

void setup() {
  vw_set_tx_pin(RF_TX_PIN); // Setup transmit pin
  vw_setup(2400); // Transmission speed in bits per second.
}

void loop() {
  int reading = analogRead(sensorPin);
  buttonState = digitalRead(buttonPin);
  
  if (buttonState == HIGH) {     
     float voltage = reading * 5.0;
     voltage /= 1024.0; 
    
     float temperatureC = (voltage - 0.5) * 100 ; 
     const char *msg = "hello hello hello";
     vw_send((uint8_t *)msg, strlen(msg));
     delay(200);
  }
  delay(100);  
}
