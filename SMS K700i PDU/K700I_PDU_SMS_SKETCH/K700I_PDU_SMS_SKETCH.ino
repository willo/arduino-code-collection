#include <SoftwareSerial.h>
SoftwareSerial phone(2,3);
const int button1Pin = 7; 
const int ledPin =  13; 

void setup()
{
  Serial.begin(9600);
  pinMode(button1Pin, INPUT);
  pinMode(ledPin, OUTPUT);
  phone.begin(9600);
  Serial.println("Ericsson K700i connected.");
  delay(1000);
  phone.println("AT+CMGF=0");
  Serial.println("Initialization...");
  delay(100);
  phone.println("AT+CPMS=\"ME\",\"ME\",\"ME\"");
  delay(500);
  //phone.println("AT+CNMI=2,3,0,0,0");
  //delay(500);
  Serial.println("Done.");
  phone.flush();  
  delay(500);
}

void loop()
{
  const String PDUheaderConst = "REPLACE-THIS-WITH-SPECIFIC-HEADER-INFORMATION";
  int button1State = digitalRead(button1Pin);
  if (phone.available() > 0)
  {
    char sms = phone.read();
    Serial.print(sms);
  }
  else
  {
    if (button1State == LOW)
    {
      digitalWrite(ledPin, HIGH);
      String convertText = "Message to be texted goes here!";
      String returnedSMS = sendSMS(convertText, PDUheaderConst);
      Serial.println(" ");
      //If function returns sucessfully (aka = 1)
      if (returnedSMS = "1")
      {
        digitalWrite(ledPin, LOW);
        digitalWrite(button1State, HIGH);
      }
      //Serial.println(returnedSMS);
      //while(1);
    }
  }
}

String sendSMS(String text, String PDUheader)
{
    char asc_msg[text.length()+1];
    text.toCharArray(asc_msg, text.length()+1);
    int getStringLength = text.length();
    int asc_len = strlen(asc_msg);
    int i, j;
    int pdu_len;
    byte outchar;
    char inchar;
    int outbit;
    String PDUoutput;
    char buffer[200];
    pdu_len = 0;
    outbit = 0;
    outchar = 0;
    for (i = 0; i < asc_len; i++) {
        inchar = asc_msg[i];
        for (j = 0; j < 7; j++) {
            outchar >>= 1;
            outchar |= (inchar & 1) << 7;
            inchar >>= 1;
            if (++outbit == 8) {
                sprintf(buffer,"%02x", outchar);
                PDUoutput += buffer;
                if (pdu_len++ % 16 == 15) {
                }
                else {
                }
                outbit = 0;
                outchar = 0;
            }
        }
    }
    if (outbit < 8) {
        ++pdu_len;
        while (outbit++ < 8) {
            outchar >>= 1;
        }
        sprintf(buffer,"%02x", outchar);
        PDUoutput += buffer;
    }
    //SMS Sending!
    //Calculate HEX on string length
    String hexStr = String(getStringLength, HEX); 
    sprintf(buffer, "%d", pdu_len);
    //Start to compose the PDU message
    phone.print("AT+CMGS=");
    //Add in PDU length
    phone.println(pdu_len + 14);
    //Thinking time!
    delay(3000);
    //Compose the PDU string
    phone.print(PDUheader+hexStr+PDUoutput);
    //Thinking time!
    delay(3000);
    //CTRL-Z (aka 26)
    phone.write(26);
    //Done! 
    return "1";
}



