# Sending SMS via a Sony Ericsson K700i #

[See blog post](http://willockenden.com) for a run down.

This code _should_ spit out compatible stings in PDU format so the K700i can send SMSes.

You'll have to modify it a bit (add your own header information). 