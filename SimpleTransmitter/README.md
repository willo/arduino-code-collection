# Simple 434 mhz transmit/receive code #

This is simple code to send/receive a simple string between two Arduinos.

Uses the VirtualWire.h library, which can be found in the Collected Libraries folder, or the latest version here: http://www.open.com.au/mikem/arduino/VirtualWire/

